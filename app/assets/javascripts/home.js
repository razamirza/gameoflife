function rand() {
  return Math.random() < .5 ? 0 : 1;
}

function init(x,y) {
// Adding 2 extra zero-paded rows and columns eliminates
// the need for array out of bound checks removing conditionals
  var state = [];
  
  // row[] = 0;
  state[0] = new Array(y+2).fill(0);
  state[x+1] = new Array(y+2).fill(0);

  for (var i = 1; i <= x; i++) {
    var row = [];
    row[0] = row[y+1] = 0;
    for (var j = 1; j <= y ; j++) {
      row[j] = rand();
    }
    state[i] = row;
  }
  return state;
}

function deadOrLive(state, i, j) {
  var count = state[i-1][j-1] + state[i-1][j] + state[i-1][j+1]
    + state[i][j-1] + state[i][j+1]
    + state[i+1][j-1] + state[i+1][j] + state[i+1][j+1];

  switch(count){
    case 2:
      return state[i][j];
    case 3:
      return 1;
    default:
      return 0;
  }
}

function next(state){
  var nState = [];
  nState[0] = new Array(state[0].length).fill(0);
  nState[state.length-1] = new Array(state[0].length).fill(0);

  for (var i = 1; i < state.length-1; i++) {
    var row = [];
    row[0] = row[state[i].length-1] = 0;
    for (var j = 1; j < state[i].length-1; j++) {
      row[j] = deadOrLive(state, i, j);
    }
    nState[i] = row;
  }
  return nState;
}

angular.module('GoL', [])
  .controller('HomeCtrl', ['$scope', function($scope) {
    $scope.dimX = 10;
    $scope.dimY = 10;
    $scope.state = init($scope.dimX, $scope.dimY);

    $scope.nextState = function() {
      $scope.state = next($scope.state);      
    };

    $scope.reset = function() {
      if($scope.dimensions.$valid) {
        $scope.state = init($scope.dimX, $scope.dimY);
      }
    };
  }]);